﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DropBlockOnHit : MonoBehaviour
{ 
	public GameObject cubePrefab;
	public GameObject[] cubes;
	private static int cubeArrayPos;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}


	IEnumerator OnCollisionEnter(Collision col)
	{	

		if (col.collider.tag == "Cube")
		{
			//Debug.Log( col.gameObject.name);

			Transform ball = gameObject.transform;
			ball.position = new Vector3(ball.position.x, ball.position.y, ball.position.z - 3f);

			for (int i = 0; i < cubes.Length; i++)
			{
				if (col.gameObject.name == cubes[i].gameObject.name)
				{
					cubeArrayPos = i;
				}
			}

			if (cubeArrayPos == 0)
			{
				for (int i = 0; i < cubes.Length; i++)
				{
					yield return new WaitForSeconds(0.25f);
					cubes[i].gameObject.GetComponent<Rigidbody>().isKinematic = false;
					if (i == 11)
					{
						yield return new WaitForSeconds(1f);

						SceneManager.LoadScene("Main");

					}
				}
			}else if (cubeArrayPos == 11)
			{
				for (int i = 11; i > -1; i--)
				{
					yield return new WaitForSeconds(0.25f);
					cubes[i].gameObject.GetComponent<Rigidbody>().isKinematic = false;
					if (i == 0)
					{
						yield return new WaitForSeconds(1f);
						SceneManager.LoadScene("Main");

					}
				}
			}else
			{
				int offsetNum = 0;
				for (int i = 0; i < cubes.Length; i++)
				{
					int posOffset = cubeArrayPos + offsetNum;
					int negOffset = cubeArrayPos - offsetNum;
					yield return new WaitForSeconds(0.25f);


					if (posOffset < 12) {
						cubes[posOffset].gameObject.GetComponent<Rigidbody>().isKinematic = false;
					}

					if (negOffset >= 0) {
						cubes[negOffset].gameObject.GetComponent<Rigidbody>().isKinematic = false;
					}
					offsetNum++;

					if (i == 11)
					{
						yield return new WaitForSeconds(1f);

						SceneManager.LoadScene("Main");

					}
				}
			}
			
		}
	}
}
