﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBall : MonoBehaviour {


	public Transform ball;
	public Rigidbody ballRigidBody;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseUpAsButton()
	{
		Vector3 mousePos = Input.mousePosition;

		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePos), out hit))
		{
			ball.position = hit.point;
			ball.position = new Vector3(ball.position.x, ball.position.y , ball.position.z - 0.5f);
			ballRigidBody.velocity = Vector3.zero;
		}
	}
}
