﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFirstRaycast : MonoBehaviour {

	public int thrust;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetMouseButtonDown(0))
		{
			Vector3 mousePos = Input.mousePosition;

			RaycastHit hit;

			Ray myRay = Camera.main.ScreenPointToRay(mousePos);

			Debug.DrawRay(myRay.origin, myRay.direction, Color.white, 1.0f);

			if (Physics.Raycast(myRay, out hit))
			{
				if (hit.collider != null)
				{
					hit.collider.gameObject.GetComponent<Renderer>().material.color = Color.red;
					hit.rigidbody.AddForceAtPosition(thrust * transform.forward, hit.point);
				}
			}
		}
	}
}
