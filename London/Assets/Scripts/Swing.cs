﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swing : MonoBehaviour {
	public int count;
	public float rotationSpeed;
	private bool isSwingingBack = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isSwingingBack)
		{
			gameObject.GetComponent<Transform>().Rotate(0, rotationSpeed, 0);
			count++;
			if (count >= 200)
			{
				isSwingingBack = !isSwingingBack;
			}
		}else
		{
			gameObject.GetComponent<Transform>().Rotate(0, -rotationSpeed, 0);
			count--;
			if (count <= 0)
			{
				isSwingingBack = !isSwingingBack;
			}
		}
	}
}
