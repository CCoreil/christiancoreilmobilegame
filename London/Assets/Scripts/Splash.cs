﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Splash : MonoBehaviour {

	public ParticleSystem effect;
	public ParticleSystem part;
	public List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

	void Start()
	{
		
	}

	void OnParticleCollision(GameObject other)
	{

		int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

		Rigidbody rb = other.GetComponent<Rigidbody>();
		int i = 0;

		while (i < numCollisionEvents)
		{
			if (rb)
			{
				ParticleSystem copy = Instantiate(effect, collisionEvents[i].intersection, new Quaternion());
				Destroy(copy.gameObject, copy.main.duration);
			}
			i++;
		}
	}
	// Use this for initialization
	



}
