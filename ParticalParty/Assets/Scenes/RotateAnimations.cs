﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RotateAnimations : MonoBehaviour {
	public string nextScene;
	public float time;
	private float timer;
	
	void Update() {
		//Debug.Log("Yo");
		timer += 0.02f;
		if (timer >= time)
		{
			SceneManager.LoadScene(nextScene);
		}
	}


}
