﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeUpdate : MonoBehaviour {
	public static double timeLeft = 11;


	// Update is called once per frame

	void Start()
	{
		gameObject.GetComponent<Text>().text = "10";
		timeLeft = 11;
	}
	
	void Update()
	{
		if (CardScript.pairsLeft != 0)
		{
			if (timeLeft <= 0)
			{
				gameObject.GetComponent<Text>().text = "Game Over!";
				timeLeft -= 0.05;
				if (timeLeft <= -2)
				{
					SceneManager.LoadScene("Title");
				}
			}
			else
			{
				timeLeft -= 0.01;
				gameObject.GetComponent<Text>().text = "" + (int)timeLeft;
			}


		}
		else
		{
			gameObject.GetComponent<Text>().text = "You Win!";
			timeLeft -= 0.1;

			if (timeLeft <= -2)
				{
					SceneManager.LoadScene("Title");
				}
			
		}
	}
}
