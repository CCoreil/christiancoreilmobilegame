﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class CardScript : MonoBehaviour
{
	public string color;
	private bool isRotate = false;
	private int degrees = 0;
	public static List<GameObject> GameObjList = new List<GameObject>();
	public static int pairsLeft = 4;
	private bool isFlipping = false;
	private bool gameStart = true;
	// Use this for initialization


	void onEnable()
	{

	}

	void Start()
	{
		pairsLeft = 4;
		degrees = 0;
		isRotate = false;

	}

	public void cardClicked()
	{
		if (!isFlipping)
		{
			isRotate = true;
			GameObjList.Add(gameObject);	
		}
		
		
	}

	void Update()
	{
		if (pairsLeft == 0)
		{
			TimeUpdate.timeLeft = -1;
		}

		if (!gameStart)
		{
			if (isRotate)
			{
				isFlipping = true;
				degrees += 5;
				gameObject.transform.Rotate(new Vector3(0, 5, 0));
				checkRotation();
			}
		}
		else
		{
			degrees += 5;
			gameObject.transform.Rotate(new Vector3(0, 5, 0));
			if (degrees == 90)
			{

				switch (color)
				{
					case "Blue":
						gameObject.GetComponent<Image>().color = Color.blue;
						break;

					case "Red":
						gameObject.GetComponent<Image>().color = Color.red;
						break;

					case "Yellow":
						gameObject.GetComponent<Image>().color = Color.yellow;
						break;

					case "Green":
						gameObject.GetComponent<Image>().color = Color.green;
						break;

					default:
						break;
				}
			}
			else if (degrees == 270)
			{
				gameObject.GetComponent<Image>().color = Color.white;
			}
			else if (degrees == 360)
			{
				degrees = 0;
				gameStart = !gameStart;
			}
		}
	}

	void checkRotation()
	{
		if (degrees == 90)
		{

			switch (color)
			{
				case "Blue":
					gameObject.GetComponent<Image>().color = Color.blue;
					break;

				case "Red":
					gameObject.GetComponent<Image>().color = Color.red;
					break;

				case "Yellow":
					gameObject.GetComponent<Image>().color = Color.yellow;
					break;

				case "Green":
					gameObject.GetComponent<Image>().color = Color.green;
					break;

				default:
					break;
			}
		}
		else if (degrees == 180)
		{
			isFlipping = false;
			isRotate = false;
			if (GameObjList.Count == 2)
			{
				if (GameObjList[0].GetComponent<Image>().color == GameObjList[1].GetComponent<Image>().color)
				{
					for (int i = 0; i < GameObjList.Count; i++)
					{
						Destroy(GameObjList[i]);
					}
					GameObjList.Clear();
					pairsLeft--;

				}
				else
				{
					for (int i = 0; i < GameObjList.Count; i++)
					{
						GameObjList[i].GetComponent<CardScript>().isRotate = true;
					}

				}
			}

		}
		else if (degrees == 270)
		{
			for (int i = 0; i < GameObjList.Count; i++)
			{
				GameObjList[i].GetComponent<Image>().color = Color.white;
			}
			GameObjList.Clear();
		}
		else if (degrees == 360)
		{
			//gameStart = false;
			isFlipping = false;
			isRotate = false;
			degrees = 0;


		}
	}
}
