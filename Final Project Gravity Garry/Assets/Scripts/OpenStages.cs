﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenStages : MonoBehaviour {


	public Button currentBtn;
	public string sceneName;
	// Use this for initialization
	void Start () {
		Button btn = currentBtn.GetComponent<Button>();
		btn.onClick.AddListener(onClickNextScene);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void onClickNextScene(){
		SceneManager.LoadScene(sceneName);
	}
}
