﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeScript : MonoBehaviour {
	private float dt;
	public float speed;

	// Use this for initialization
	void Start () {
		dt = Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		dt = Time.deltaTime;
		gameObject.GetComponent<Transform>().Rotate(0, 0, dt * speed);
	}
}
