﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerWall : MonoBehaviour {

	public int wallType;
	public float speed = 20;
	private float dt;
	public float totalSeconds;
	private float totalDistance;
	private float distance;
	private bool movingLeft = true;
	// Use this for initialization
	void Start () {
		dt = Time.deltaTime;
		distance = totalSeconds * Time.deltaTime;
		totalDistance = totalSeconds * Time.deltaTime;
		
	}
	
	// Update is called once per frame
	void Update () {
		dt = Time.deltaTime;

		switch (wallType)
		{
			case 1:
				gameObject.GetComponent<Transform>().Rotate(0, 0, dt * speed);
				break;

			case 2:
				if (movingLeft)
				{
					gameObject.GetComponent<Transform>().Translate(0, dt * -speed, 0);
					distance-=Time.deltaTime;
					if (distance <= 0)
					{
						movingLeft = !movingLeft;
					}
				}
				else
				{
					gameObject.GetComponent<Transform>().Translate(0, dt * speed, 0);
					distance+=Time.deltaTime;
					if (distance >= totalSeconds)
					{
						movingLeft = !movingLeft;
					}
				}
				break;

			default:
				//Debug.Log("Default Lazerwall that does not move");
				break;
		}
	}
}
