﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour {
	public string sceneName;
	public float gravityPull;
	public float speed;
	private static float dt;
	private static float areaWidth;
	private static float areaHeight;
	public static Vector3 playerPosition = new Vector3(-1.68f,-0.4f,0);
	public static Vector3 cameraPosition = new Vector3(0, 0, -10);
	public GameObject[] checkpoints;




	bool isGravNormal = true;
	bool isMovingRight = false;
	bool isMovingLeft = false;
	bool isAirDodgeing = false;

	// Use this for initialization
	//void Start()
	//{
	//	checkpoints = GameObject.FindGameObjectsWithTag("checkpoint");
	//	dt = Time.deltaTime;
	//	areaWidth = 3.721f;
	//	areaHeight = 1.92f;
	//	gameObject.GetComponent<Transform>();
	//	transform.position = playerPosition;
	//	Camera.main.transform.position += cameraPosition;
	//}

	void Awake()
	{
		checkpoints = GameObject.FindGameObjectsWithTag("checkpoint");
		dt = Time.deltaTime;
		areaWidth = 3.721f;
		areaHeight = 1.92f;
		gameObject.GetComponent<Transform>();
		transform.position = playerPosition;
		Camera.main.transform.position = cameraPosition;
	}

	// Update is called once per frame
	void Update () {
		dt = Time.deltaTime;

		checkConditions();
		changeMotion();
	}

	void checkConditions()
	{
		if (Input.GetKeyDown("w") || Input.GetKeyDown("up"))
		{
			isGravNormal = !isGravNormal;
		}

		if (Input.GetKeyDown("d") || Input.GetKeyDown("right"))
		{
			isMovingRight = true;
		}
		else if (Input.GetKeyUp("d") || Input.GetKeyUp("right"))
		{
			isMovingRight = false;
		}

		if (Input.GetKeyDown("a") || Input.GetKeyDown("left"))
		{
			isMovingLeft = true;
		}
		else if (Input.GetKeyUp("a") || Input.GetKeyUp("left"))
		{
			isMovingLeft = false;
		}

		if (Input.GetKeyDown("e") || Input.GetKeyDown("page up"))
		{
			if (!isAirDodgeing) {
				isAirDodgeing = true;
				gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;

			}
		}

		if (Input.GetKeyDown("escape"))
		{
			playerPosition = new Vector3(-1.68f, -0.4f, 0);
			cameraPosition = new Vector3(0, 0, -10);
			SceneManager.LoadScene("LevelMenu");
		}


	}

	void changeMotion()
	{
		if (!isAirDodgeing) {
			if (!isGravNormal)
			{
				gameObject.GetComponent<Rigidbody2D>().gravityScale = -gravityPull;
				gameObject.GetComponent<SpriteRenderer>().flipY = true;
			}
			else
			{
				gameObject.GetComponent<Rigidbody2D>().gravityScale = gravityPull;
				gameObject.GetComponent<SpriteRenderer>().flipY = false;
			}

			if (isMovingLeft)
			{
				gameObject.GetComponent<Rigidbody2D>().position -= new Vector2(speed * dt, 0);
				gameObject.GetComponent<SpriteRenderer>().flipX = true;
			}

			if (isMovingRight)
			{
				gameObject.GetComponent<Rigidbody2D>().position += new Vector2(speed * dt, 0);
				gameObject.GetComponent<SpriteRenderer>().flipX = false;
			}

		}
		else { 

			//Debug.Log("Before Call");

			StartCoroutine(airDodge());

			//Debug.Log("After Call");

		}
	}

	public IEnumerator airDodge()
	{
		gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
		yield return new WaitForSeconds(0.5f);
		gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
		isAirDodgeing = false;
		gameObject.GetComponent<SpriteRenderer>().color = Color.white;
		




		//Debug.Log("After wait");
		//gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
		//isAirDodgeing = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
			if (other.gameObject.CompareTag("KillItem") )
			{
				if (!isAirDodgeing)
				{
					Refresh.sceneName = this.sceneName;
					SceneManager.LoadScene("GameOver");
				}
			}else if (other.gameObject.CompareTag("winZone"))
			{
				playerPosition = new Vector3(-1.68f, -0.4f, 0);
				cameraPosition = new Vector3(0, 0, -10);
			SceneManager.LoadScene("WIN");
			}

			if (other.gameObject.CompareTag("moveRight"))
			{
				gameObject.transform.position += new Vector3(0.3f, 0, 0);
				Camera.main.transform.position += new Vector3(areaWidth, 0, 0);
			}
			else if (other.gameObject.CompareTag("moveLeft"))
			{
				gameObject.transform.position -= new Vector3(0.3f, 0, 0);
				Camera.main.transform.position -= new Vector3(areaWidth, 0, 0);
			}
			else if (other.gameObject.CompareTag("moveUp"))
			{
				gameObject.transform.position += new Vector3(0, 0.5f, 0);
				Camera.main.transform.position += new Vector3(0, areaHeight, 0);
			}
			else if (other.gameObject.CompareTag("moveDown"))
			{
				gameObject.transform.position -= new Vector3(0, 0.5f, 0);
				Camera.main.transform.position -= new Vector3(0, areaHeight, 0);
			}
			else if(other.gameObject.CompareTag("checkpoint"))
			{
				Vector3 newPostion = new Vector3(other.transform.position.x, other.transform.position.y, 0);
				playerPosition = newPostion;
				cameraPosition = Camera.main.transform.position;


				foreach (GameObject obj in checkpoints)
				{
					obj.GetComponent<SpriteRenderer>().color = Color.gray;
				}

				other.GetComponent<SpriteRenderer>().color = Color.green;
			}
		else if (other.gameObject.CompareTag("checkpointupsidedown"))
		{
			Vector3 newPostion = new Vector3(other.transform.position.x, other.transform.position.y, 0);
			playerPosition = newPostion;
			cameraPosition = Camera.main.transform.position;
			isGravNormal = false;

			foreach (GameObject obj in checkpoints)
			{
				obj.GetComponent<SpriteRenderer>().color = Color.gray;
			}

			other.GetComponent<SpriteRenderer>().color = Color.green;
		}
		
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("KillItem"))
		{
			if (!isAirDodgeing)
			{
				Refresh.sceneName = this.sceneName;
				SceneManager.LoadScene("GameOver");
			}
		}
	}

	


}
