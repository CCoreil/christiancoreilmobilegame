﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ReturnToLevels : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("escape"))
		{
			PlayerScript.playerPosition = new Vector3(-1.68f, -0.4f, 0);
			PlayerScript.cameraPosition = new Vector3(0, 0, -10);
			SceneManager.LoadScene("LevelMenu");
		}
	}
}
